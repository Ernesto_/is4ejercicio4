<?php
try {
    $pdo = new PDO('pgsql:host=localhost;dbname=login', 'tu_usuario', 'tu_contraseña');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Error de conexión a la base de datos: " . $e->getMessage());
}
?>
