<?php
session_start();

require_once('db.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $usuario = $_POST['usuario'];
    $password = $_POST['password'];

    // Encripta la contraseña antes de buscarla en la base de datos (puedes usar password_hash)
    // Aquí se usa password_hash para demostrar un ejemplo, asegúrate de usar un algoritmo adecuado
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

    // Consulta la base de datos para verificar el usuario y la contraseña
    $stmt = $pdo->prepare("SELECT * FROM users WHERE nombre_usuario = ?");
    $stmt->execute([$usuario]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($user && password_verify($password, $user['password'])) {
        // Usuario válido, establece variables de sesión
        $_SESSION['nombre'] = $user['nombre'];
        $_SESSION['apellido'] = $user['apellido'];
        header("Location: welcome.php");
        exit();
    } else {
        $error = "Usuario o contraseña incorrectos.";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Iniciar Sesión</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
    <div class="login-container">
        <h2>Iniciar Sesión</h2>
        <?php if (isset($error)) : ?>
            <p class="error-message"><?php echo $error; ?></p>
        <?php endif; ?>
        <form action="login.php" method="post">
            <label for="usuario">Usuario:</label>
            <input type="text" id="usuario" name="usuario" required><br><br>
            <label for="password">Contraseña:</label>
            <input type="password" id="password" name="password" required><br><br>
            <button type="submit">Iniciar Sesión</button>
        </form>
    </div>
</body>
</html>
